require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AppName
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # redis用
    config.cache_store = :redis_store, Rails.application.credentials.redis[:cache], { expires_in: 90.minutes }    
    # 日本語設定
    config.i18n.default_locale = :ja
    config.time_zone = 'Tokyo'
  end
end
