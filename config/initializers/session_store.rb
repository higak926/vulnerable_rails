AppName::Application.config.session_store :redis_store, {
  servers: [
    {
      host: Rails.application.credentials.redis[:host],
      port: Rails.application.credentials.redis[:port],
      db: Rails.application.credentials.redis[:db],
      namespace: Rails.application.credentials.redis[:namespace]
    },
  ],
  # 設定時間
  expire_after: 1.minute,
  key: "_#{Rails.application.class.parent_name.downcase}_session"
}
