module Features

  module SessionHelpers

    def sign_up_with(email, password, confirmation)
      visit new_user_registration_path
      fill_in "メールアドレス", with: email
      fill_in "パスワード", with: password
      fill_in "確認用パスワード", with: confirmation
      click_button "Sign up"
    end

    def sign_in(email, password)
      visit new_user_session_path
      fill_in "メールアドレス", with: email
      fill_in "パスワード", with: password
      click_button "ログイン"
    end
  end
end