class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
     # 新規登録時(sign_up時)にnameというキーのパラメーターを追加で許可する
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  def after_sign_in_path_for(resource)
    if current_user
      flash[:notice] = "ログインしました。" 
      root_url
    else
      flash[:notice] = "新規登録完了しました。" 
      root_url
    end
  end

  def after_sign_out_path_for(resource)
    flash[:notice] = "ログアウトしました。" 
    root_url
  end

end
